import Reveal from "reveal.js";
import hljs from "highlight.js";

const RevealHighlight = {
    init: () => {
        document.querySelectorAll( '.reveal pre code' ).forEach(
            (block) => {
                block.addEventListener(
                    'focusout',
                    (event) => hljs.highlightBlock(event.currentTarget),
                    false
                );
                hljs.highlightBlock(block);
            }
        );
    },
};

function main() {
    if (document.readyState !== 'complete') {
        setTimeout(main, 50);
        return;
    }

    Reveal.registerPlugin('highlight', RevealHighlight);

    Reveal.initialize({
        controls: true,
        controlsLayout: 'bottom-right',
        controlsBackArrows: 'faded',
        keyboard: true,

        progress: true,
        slideNumber: false,

        hash: true,
        history: true,

        overview: true,
        center: true,
    });

    document.addEventListener('keydown', ({keyCode}) => {
        if (keyCode === 69) {
            window.open('https://runkit.com/bjpbakker/fpjs-overture', '_blank');
        }
    });
}

main();
